# TritonApiSample
* Triton Api Server에서 제공하는 REST API를 사용하는 샘플

### **Install**

> 가상환경 구성 (최초 1회)
```bash
pip3 install virtualenv
virtualenv venv --python=python3
```

> 가상환경 사용
```bash
source venv/bin/activate
```

> 사용 모듈 설치
```bash
pip install -r requirements.txt
```

### **Configuration**
* sample.py 코드 상단에 있는, CONST_API_SERVER_URL과 CONST_API_KEY 2개의 값을 CUBOX AILab으로부터 전달받은 값으로 수정

### **Run**

> 프로그램 실행
```bash
python sample.py
python sample_secure.py
```