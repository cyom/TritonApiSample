import base64
import hashlib
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP, PKCS1_v1_5

_AES_BS_ = AES.block_size
class AESCipher(object):
    def __init__(self, key, iv = bytes(16), v1 = True):
        if v1:
            self.key = key.encode()
        else:
            self.key = hashlib.sha256(key.encode()).digest()
        
        self.iv = iv

    def encrypt(self, raw):
        raw = self._pad(raw)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return base64.b64encode(cipher.encrypt(raw.encode()))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_CBC, self.iv)
        return self._unpad(cipher.decrypt(enc)).decode('utf-8')

    @staticmethod
    def _pad(s):
        return s + (_AES_BS_ - len(s) % _AES_BS_) * chr(_AES_BS_ - len(s) % _AES_BS_)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


class Pkcs1Cipher(object):
    #. RSA 암호화 가능한 길이는 245 byte 이므로 얼굴이미지 자체를 암호화할 수 없다.
    #. 따라서, 얼굴이미지는 AES-256 암호화가 적용되며, AES 대칭키를 암호화하기 위한 수단으로 상기 RSA 암호화를 사용한다.

    with open("public.pem", "rb") as pu:
        pub_key = RSA.importKey(pu.read())
        cipher_public = PKCS1_OAEP.new(pub_key)

    def encrypt(self, key):
        return self.cipher_public.encrypt(key.encode())

#. 대칭키 암호화 (공개키로 RSA 암호화)
def get_symmetric_key_enc(aes_key):
    pkcs1 = Pkcs1Cipher()
    sym_key_enc = pkcs1.encrypt(aes_key)
    sym_key_enc = base64.b64encode(sym_key_enc).decode()
    return sym_key_enc