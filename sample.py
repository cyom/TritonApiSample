import base64
import requests, json

CONST_API_SERVER_URL = "https://xxx.xxx.xxx"
CONST_API_KEY = "Enter the value you received."

def get_detect(jpg):
    req_data = {
        "seq": 1,
        "maxResults": 1000,
        "image": base64.b64encode(jpg).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v1/detect", headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY}, data=json.dumps(req_data))
    if (res.status_code == 200):
        return True, res.json()
    else:
        print(f'Status Code={res.status_code}')
        return False, None

def get_embeddings(jpg):
    req_data = {
        "image": base64.b64encode(jpg).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v1/embeddings", headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY}, data=json.dumps(req_data))
    if (res.status_code == 200):
        return True, res.json()
    else:
        print(f'Status Code={res.status_code}')
        return False, None

def get_feature(jpg):
    req_data = {
        "image": base64.b64encode(jpg).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v1/feature", headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY}, data=json.dumps(req_data))
    if (res.status_code == 200):
        ver = res.json()['version']
        feature = res.json()['feature']
        face_detect = res.json()['face_detect']
        return True, feature, face_detect
    else:
        print(f'Status Code={res.status_code}')
        return False, None, None

def get_score(jpg1, jpg2):
    req_data = {
        "image1": base64.b64encode(jpg1).decode(),
        "image2": base64.b64encode(jpg2).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v1/score", headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY}, data=json.dumps(req_data))
    if (res.status_code == 200):
        score = res.json()['score']
        face_detects = res.json()['face_detects']
        return True, score, face_detects
    else:
        print(f'Status Code={res.status_code}')
        return False, None, None

def get_feature_score(feature1, feature2):
    req_data = {
        "feature1": feature1,
        "feature2": feature2
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v1/feature-score", headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY}, data=json.dumps(req_data))
    if (res.status_code == 200):
        score = res.json()['score']
        return True, score
    else:
        print(f'Status Code={res.status_code}')
        return False, None

def get_feature_image_score(feature, jpg):
    req_data = {
        "src_feature": feature,
        "dst_image": base64.b64encode(jpg).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v1/feature-image-score", headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY}, data=json.dumps(req_data))
    if (res.status_code == 200):
        score = res.json()['score']
        ver = res.json()['dst_feature_version']
        feature = res.json()['dst_feature']
        return True, score, feature
    else:
        print(f'Status Code={res.status_code}')
        return False, None, None

if __name__ == '__main__':

    with open('woman.jpg', 'rb') as f1, open('woman_r.jpg', 'rb') as f2:
        jpg1 = f1.read()
        jpg2 = f2.read()

        ret, detect = get_detect(jpg1)
        print(f'(detect) ret={ret}')
        # print(f'(detect) ret={ret} detect={detect}')

        ret, detect = get_embeddings(jpg1)
        print(f'(embeddings) ret={ret}')
        # print(f'(embeddings) ret={ret} detect={detect}')

        ret, feat1, detect = get_feature(jpg1)
        print(f'(feature) ret={ret}')
        # print(f'(feature) ret={ret} feature1={feat1} detect={detect}')

        ret, feat2, detect = get_feature(jpg2)
        print(f'(feature) ret={ret}')
        # print(f'(feature) ret={ret} feature2={feat2} detect={detect}')

        ret, score, detect = get_score(jpg1, jpg2)
        print(f'(score) ret={ret} score={score}')
        # print(f'(score) ret={ret} score={score} detect={detect}')

        ret, score = get_feature_score(feat1, feat2)
        print(f'(feature-score) ret={ret} score={score}')
        # print(f'(feature-score) ret={ret} score={score}')

        ret, score, feat = get_feature_image_score(feat1, jpg2)
        print(f'(feature-image-score) ret={ret} score={score}')
        # print(f'(feature-image-score) ret={ret} score={score} feature={feat}')

