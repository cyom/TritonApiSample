import random
import base64
import requests, json
from encrypt import AESCipher, get_symmetric_key_enc

CONST_API_SERVER_URL = "https://xxx.xxx.xxx"
CONST_API_KEY = "Enter the value you received."

def get_detect_secure(jpg, aes_key):
    cipher = AESCipher(aes_key, bytes(16), False)
    req_data = {
        "seq": 1,
        "maxResults": 1000,
        "image": cipher.encrypt(base64.b64encode(jpg).decode()).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v2/detect-secure", 
        headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY, 'x-secure-key': get_symmetric_key_enc(aes_key)}, 
        data=json.dumps(req_data))
    if (res.status_code == 200):
        return True, res.json()
    else:
        print(f'Status Code={res.status_code}')
        return False, None

def get_embeddings_secure(jpg, aes_key):
    cipher = AESCipher(aes_key, bytes(16), False)
    req_data = {
        "image": cipher.encrypt(base64.b64encode(jpg).decode()).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v2/embeddings-secure", 
        headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY, 'x-secure-key': get_symmetric_key_enc(aes_key)}, 
        data=json.dumps(req_data))
    if (res.status_code == 200):
        return True, res.json()
    else:
        print(f'Status Code={res.status_code}')
        return False, None

def get_feature_secure(jpg, aes_key):
    cipher = AESCipher(aes_key, bytes(16), False)
    req_data = {
        "image": cipher.encrypt(base64.b64encode(jpg).decode()).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v2/feature-secure", 
        headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY, 'x-secure-key': get_symmetric_key_enc(aes_key)}, 
        data=json.dumps(req_data))
    if (res.status_code == 200):
        ver = res.json()['version']
        feature = res.json()['feature']
        face_detect = res.json()['face_detect']
        return True, feature, face_detect
    else:
        print(f'Status Code={res.status_code}')
        return False, None, None

def get_score_secure(jpg1, jpg2, aes_key):
    cipher = AESCipher(aes_key, bytes(16), False)
    req_data = {
        "image1": cipher.encrypt(base64.b64encode(jpg1).decode()).decode(),
        "image2": cipher.encrypt(base64.b64encode(jpg2).decode()).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v2/score-secure", 
        headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY, 'x-secure-key': get_symmetric_key_enc(aes_key)}, 
        data=json.dumps(req_data))
    if (res.status_code == 200):
        score = res.json()['score']
        face_detects = res.json()['face_detects']
        return True, score, face_detects
    else:
        print(f'Status Code={res.status_code}')
        return False, None, None

def get_feature_score_secure(feature1, feature2, aes_key):
    cipher = AESCipher(aes_key, bytes(16), False)
    req_data = {
        "feature1": cipher.encrypt(feature1).decode(),
        "feature2": cipher.encrypt(feature2).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v2/feature-score-secure", 
        headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY, 'x-secure-key': get_symmetric_key_enc(aes_key)}, 
        data=json.dumps(req_data))
    if (res.status_code == 200):
        score = res.json()['score']
        return True, score
    else:
        print(f'Status Code={res.status_code}')
        return False, None

def get_feature_image_score_secure(feature, jpg, aes_key):
    cipher = AESCipher(aes_key, bytes(16), False)
    req_data = {
        "src_feature": cipher.encrypt(feature).decode(),
        "dst_image": cipher.encrypt(base64.b64encode(jpg).decode()).decode()
    }
    res = requests.post(f"{CONST_API_SERVER_URL}/v2/feature-image-score-secure", 
        headers={'Content-Type': 'application/json', 'x-api-key': CONST_API_KEY, 'x-secure-key': get_symmetric_key_enc(aes_key)}, 
        data=json.dumps(req_data))
    if (res.status_code == 200):
        score = res.json()['score']
        ver = res.json()['dst_feature_version']
        feature = res.json()['dst_feature']
        return True, score, feature
    else:
        print(f'Status Code={res.status_code}')
        return False, None, None

if __name__ == '__main__':

    #. 클라이언트: 랜덤 대칭키 생성
    aes_key = ''.join(random.SystemRandom().choices('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', k=16))
    print(aes_key)

    with open('woman.jpg', 'rb') as f1, open('woman_r.jpg', 'rb') as f2:
        jpg1 = f1.read()
        jpg2 = f2.read()

        ret, detect = get_detect_secure(jpg1, aes_key)
        print(f'(v2/detect-secure) ret={ret}')
        # print(f'(detect-secure) ret={ret} detect={detect}')

        ret, detect = get_embeddings_secure(jpg1, aes_key)
        print(f'(v2/embeddings-secure) ret={ret}')
        # print(f'(embeddings-secure) ret={ret} detect={detect}')

        ret, feat1, detect = get_feature_secure(jpg1, aes_key)
        print(f'(v2/feature-secure) ret={ret}')
        # print(f'(feature-secure) ret={ret} feature1={feat1} detect={detect}')

        ret, feat2, detect = get_feature_secure(jpg2, aes_key)
        print(f'(v2/feature-secure) ret={ret}')
        # print(f'(feature-secure) ret={ret} feature2={feat2} detect={detect}')

        ret, score, detect = get_score_secure(jpg1, jpg2, aes_key)
        print(f'(v2/score-secure) ret={ret} score={score}')
        # print(f'(score-secure) ret={ret} score={score} detect={detect}')

        ret, score = get_feature_score_secure(feat1, feat2, aes_key)
        print(f'(v2/feature-score-secure) ret={ret} score={score}')
        # print(f'(feature-score-secure) ret={ret} score={score}')

        ret, score, feat = get_feature_image_score_secure(feat1, jpg2, aes_key)
        print(f'(v2/feature-image-score-secure) ret={ret} score={score}')
        # print(f'(feature-image-score-secure) ret={ret} score={score} feature={feat}')
        