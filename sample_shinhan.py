import requests

CONST_API_SERVER_URL = "https://xxx.xxx.xxx"
CONST_API_KEY = "Enter the value you received."

with open('woman.jpg', 'rb') as f1, open('woman_r.jpg', 'rb') as f2:
    res = requests.post(
        f"{CONST_API_SERVER_URL}/v1/shinhan/score",
        headers={'x-api-key': CONST_API_KEY},
        files={
            "image1": f1,
            "image2": f2
        },
    )
    if (res.status_code == 200):
        score = res.json()['score']
        face_detects = res.json()['face_detects']
        print(f'Status Code={res.status_code} Score={score}')
    else:
        print(f'Status Code={res.status_code}')